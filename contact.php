<?php
include('include/header.php');
?>
<!-- banner -->
<section class="inner-page-banner" id="home">
</section>
<!-- //banner -->

<!-- page details -->
<div class="breadcrumb-agile">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
    </ol>
</div>
<!-- //page details -->
<!-- //banner-botttom -->
<section class="content-info py-5">
    <div class="container py-md-5">
        <div class="text-center px-lg-5">
            <h3 class="heading text-center mb-3 mb-sm-5">Contact Us</h3>
            
        </div>
        <div class="contact-w3pvt-form mt-5">
            <form action="#" class="w3layouts-contact-fm" method="post">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>First Name & Last Name</label>
                            <input class="form-control" type="text" name="Name" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input class="form-control" type="email" name="Name" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label>City</label>
                            <input class="form-control" type="text" name="Name" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label>State</label>
                            <input class="form-control" type="text" name="Email" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label>Country</label>
                            <input class="form-control" type="text" name="Email" placeholder="" required="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Company Name</label>
                            <input class="form-control" type="text" name="Email" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input class="form-control" type="text" name="Name" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label>Write Message</label>
                            <textarea rows="8" class="form-control" name="Message" placeholder=""  required=""></textarea>
                        </div>
                    </div>
                    <div class="form-group mx-auto mt-3">
                        <button type="submit" class="btn submit">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>
<!-- //banner-botttom -->

<div class="map-w3layouts">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3680.681795383659!2d70.78441121491335!3d22.7028865851159!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959ece7c0426985%3A0xf603ee70ee3c23a1!2sNEW%20VALLEY%20TEXTILE!5e0!3m2!1sen!2sin!4v1594637505110!5m2!1sen!2sin"
        width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>
</div>
<!-- footer -->

<!-- //footer -->
<?php include("include/footer.php"); ?>

    <!-- //footer -->

</body>

</html>