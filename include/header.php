<!--
Author:W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Valley Textile</title>
    <link rel="icon" type="image/png" href="images/valley.png">
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Valley Textile">
    <meta charset="UTF-8" />
    <meta name="keywords"
        content="Valley Textile,Textile Industry,Velcro,Velcro In Morbi, Velcro In India,Valley Textile In Morbi,Valley Textile In India,Nylon Polyster Mix Tape,Nylon Polyster Mix Tape In Morbi,Nylon Polyster Mix Tape In India,Hook and Loop,Hook and Loop In Morbi,Hook and Loop In India,Hook and Loop Manufacturer,Hook and Loop Manufacturer In Morbi,Hook and Loop Manufacturer In India,High Strip Tape,High Strip Tape In Morbi,High Strip Tape IN India,Fire Retatdant
,Fire Retatdant In Morbi,Fire Retatdant In India,Nylon Tape,Nylon Tape In Morbi,Nylon Tape In India,Soft Hook Tape,Soft Hook Tape In India,Soft Hook Tape In Morbi,Unnapped Loop,Unnapped Loop In Morbi,Unnapped Loop In India,Color Hook & Loop Tape,Color Hook & Loop Tape In Morbi,Color Hook & Loop Tape In India,Premium Adhesive Tapes,Premium Adhesive Tapes
 In Morbi,Premium Adhesive Tapes In India,Regular Adhesive Tapes,Regular Adhesive Tapes In Morbi,Regular Adhesive Tapes In India,Adhesive Piece Cuts,Adhesive Piece Cuts In Morbi,Adhesive Piece Cuts In India,Adhesive Coins,Adhesive Coins In India,Adhesive Coins In Morbi,Inject / Moulded Hook,Inject / Moulded Hook In Morbi,Inject / Moulded Hook in India" />
    <script>
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>

    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->

    <link href="https://fonts.googleapis.com/css2?family=Saira+Condensed:wght@700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link href="//fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
        rel="stylesheet">
    <!-- //Fonts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!--Floating WhatsApp css-->
    <link rel="stylesheet"
        href="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.css">
    <!--Floating WhatsApp javascript-->
    <script type="text/javascript"
        src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js">
    </script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>


</head>

<body>
    <!-- header -->
    <header>
        <div class="container">

            <div class="header d-lg-flex justify-content-between align-items-center">
                <div class="header-agile">
                    <h1 >
                        <a class="navbar-brand logo" href="index.php" style="font-family: 'Saira Condensed', sans-serif;" >
                            <span aria-hidden="true">
                                <i>
                            </span>Valley <SUB><BR> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Textile</BR></SUB></i> <span aria-hidden="true"></span>
                        </a>
                    </h1>
                </div>
                <div class="nav_w3ls">
                    <nav>
                        <label for="drop" class="toggle mt-lg-0 mt-1"><span class="fa fa-bars"
                                aria-hidden="true"></span></label>
                        <input type="checkbox" id="drop" />
                        <ul class="menu">
                            <li class="mr-lg-3 mr-2 active"><a href="index.php">Home</a></li>
                            <li class="mr-lg-3 mr-2 p-0">
                                <!-- First Tier Drop Down -->
                                <label for="drop-2" class="toggle" style="font-family: 'Oswald', sans-serif;">Product <span class="fa fa-angle-down"
                                        aria-hidden="true"></span> </label>
                                <a href="#">Product <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                                <input type="checkbox" id="drop-2" />
                                <ul class="inner-dropdown">
                                    <li><a href="hook_look.php" style="text-decoration: none;">Hook & Loop Tape</a></li>
                                    <li><a href="adhesive_tape.php" style="text-decoration: none;">Adhesive Tape</a>
                                    </li>
                                    <li><a href="special_product.php" style="text-decoration: none;">Special Product</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mr-lg-3 mr-2"><a href="application.php">Application</a></li>
                            <li class="mr-lg-3 mr-2"><a href="blog_post.php">Blog & Post</a></li>
                            <li class="mr-lg-3 mr-2"><a href="contact.php">Contact Us</a></li>


                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </header>
    <!-- //header -->