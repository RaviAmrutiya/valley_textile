<?php
include('include/header.php');
?>


<!-- banner -->
<section class="inner-page-banner" id="home">
</section>
<!-- //banner -->
<!-- page details -->
<div class="breadcrumb-agile">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Special Product </li>
    </ol>
</div>
<!-- //page details -->
<!--about-mid -->
<section class="banner-bottom py-5" id="exp">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Special Product</h3>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col-md-4">
                <div class="card">
                    <img src="images/special.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Inject / Moulded Hook</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel1">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel1" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Inject / Moulded Hook
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Hook- Mouled/Injection Hook</b></h4>
                                        </center> <br>
                                        <p>
                                            India's First Manufacture.
                                            It is extremely soft in texture that prevents any injury or rashes to baby
                                            skin.It is utilized for closure in many Infant Wear
                                            Products, and Accessories like in Diapers and Nappies.
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> APPARELS, INFANT WEAR, ORTHOPEDIC, SPECIALITY
                                            PRODUCTS, SPORTS , Hygeen </p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colours – Black, White and Transparent </p>
                                        <h5><b>available Size :</b></h5><br><p>
                                        10mm to 160mm
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/fire.jpg" class="card-img-top" alt="Fire Retardant Hook and Loop">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Fire Retardant</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel3">
                                View More
                            </button>
                        </center>
                        <!-- Modal -->
                        <div class="modal fade" id="mymodel3" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Fire Retatdant
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Fire Retardant Hook and Loop</b></h4>
                                        </center> <br>
                                        <p>
                                            Fire Retardant is used
                                            across different industries with applications like personal and safety
                                            equipment, defense
                                            and military wear,
                                            and Garments. available in Antique Brand and Ronak Brand
                                        </p><br>
                                        <p> <h5><b> CATEGORIES:</b></h5> <br> AVIATION, DEFENSE, military Ware</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors – Black & White <br>Dye-To-Match
                                            colors available upon request </p><br>
                                        <p><h5><b>TECH SPECS:</b></h5> <br> 
                                            Test Certificate available upon request.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/soft_hook.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Soft Hook tape</h3>
                            </b>
                            <p class="card-text"></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel2">
                                View More
                            </button>
                        </center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel2" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Soft Hook tape
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Ronak Soft Hook tape</b></h4>
                                        </center> <br>
                                        <p>
                                           Soft Hook Tape Using in Sports.
                                        </p><br>
                                    
                                         Standard Colors – Black & White </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row row-cols-1 row-cols-md-3" style="margin-top: 20px;">
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/adheshiv_coin.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Unnapped Loop</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel4">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel4" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Regular Adhesive Tape
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Regular Hook and Loop Adhesive Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Adhesive Piece Cuts are hook and loop square cuts. It comes in both
                                            adhesive and
                                            non-adhesive forms. The Hook and Loop pieces are provided separately.
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> PACKAGING, PRESSURE SENSITIVE HOOK AND LOOP,
                                            STATIONERY</p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colors – Black & White </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/waldebale.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Weldable Hook and Loop Tape</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel4">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel4" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Regular Adhesive Tape
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Regular Hook and Loop Adhesive Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Adhesive Piece Cuts are hook and loop square cuts. It comes in both
                                            adhesive and
                                            non-adhesive forms. The Hook and Loop pieces are provided separately.
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> PACKAGING, PRESSURE SENSITIVE HOOK AND LOOP,
                                            STATIONERY</p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colors – Black & White </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<!-- //about-mid -->

<!--//team -->
<!-- <section class="banner-bottom  py-5">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Our Team</h3>
        <div class="row mt-lg-5 mt-4">
            <div class="col-md-4 team-gd text-center">
                <div class="team-img mb-4">
                    <img src="images/t1.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3">JAMES Men spa</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-md-4 team-gd second text-center my-md-0 my-5">
                <div class="team-img mb-4">
                    <img src="images/t2.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3">DEEN MUSTACHIO</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-4 team-gd text-center">
                <div class="team-img mb-4">
                    <img src="images/t3.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3"> CLINT HAIRISTA</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

    </div>
</section> -->
<!--//team -->
<!-- footer -->
<?php include("include/footer.php"); ?>

<!-- //footer -->
</body>

</html>