<?php
include('include/header.php');
?>


<!-- banner -->
<section class="inner-page-banner" id="home">
</section>
<!-- //banner -->
<!-- page details -->
<div class="breadcrumb-agile">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Adhesive Tape</li>
    </ol>
</div>
<!-- //page details -->
<!--about-mid -->
<section class="banner-bottom py-5" id="exp">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Adhesive Tape</h3>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/Valley_Roll_8 Corner_Design.jpg" class="card-img-top" alt="Premium Adhesive Tape">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Premium Adhesive Tape</h3>
                            </b>
                            <p class="card-text"></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel1">
                                View More
                            </button>
                        </center>
                        <!-- Modal -->
                        <div class="modal fade" id="mymodel1" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Premium Adhesive Tape
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Industrial Grade Self-Adhesive Hook and Loop Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Premium Adhesive Tape one of our hook and loop adhesive tapes. It has a unique
                                            adhesive that ensures proper closure/bonding in various Pop-Up displays and
                                            stands as well as packaging products.
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> Automotive, Avation, packaging,
                                            Pressure Sensitive Hook And Loop, Stationary , Ceramic Tiles Display
                                        </p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colors – Black & White.
                                        <br><h5><b>Available Size :</b></h5>
                                         <br>20mm , 25mm , 50mm </p>
                                          </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/stick_on.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Regular Adhesive Tape</h3>
                            </b>
                            <p class="card-text"></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel2">
                                View More
                            </button>
                        </center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel2" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Regular Adhesive Tape
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Regular Hook and Loop Adhesive Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Regular Adhesive Tape is another hook and loop adhesive-backed tape.it is Commercial Product.
                                         
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> packaging,
                                            Stationary</p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colors – Black & White </p>
                                        <h5><b>Available Size :</b></h5><p> 20mm , 25mm , 50mm </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/adhesive.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Adhesive Piece Cuts</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel3">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel3" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Adhesive Piece Cuts
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Regular Hook and Loop Adhesive Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Adhesive Piece Cuts are square cuts , Round Shape , Oval Shape . It comes in both
                                            adhesive.
                                        The Hook and Loop pieces are provided separately.
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> Stationary
                                            </p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colors – Black & White </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3" style="margin-top: 20px;">
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/adheshiv_coin.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Adhesive Coins</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel4">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel4" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Adhesive Coins
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                       
                                        <p>
                                            Adhesive Coins is Simple peel and stick application. Great for crafting and small projects around the home, classroom and office.
                                            Great for school projects, organization around the home and office.
                                        </p><br>
                                        <p> <b> CATEGORIES:</b> <br> Stationary, Toys</p><br>
                                        <p><b>AVAILABILITY:</b> <br>Standard Colors – Black & White </p>
                                        <h5><b>Available Size :</b></h5><p> 13mm , 16mm , 18mm </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //about-mid -->
<!--//states -->

<!--//team -->
<!-- <section class="banner-bottom  py-5">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Our Team</h3>
        <div class="row mt-lg-5 mt-4">
            <div class="col-md-4 team-gd text-center">
                <div class="team-img mb-4">
                    <img src="images/t1.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3">JAMES Men spa</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-md-4 team-gd second text-center my-md-0 my-5">
                <div class="team-img mb-4">
                    <img src="images/t2.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3">DEEN MUSTACHIO</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-4 team-gd text-center">
                <div class="team-img mb-4">
                    <img src="images/t3.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3"> CLINT HAIRISTA</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

    </div>
</section> -->
<!--//team -->
<!-- footer -->
<?php include("include/footer.php"); ?>

<!-- //footer -->



</body>

</html>