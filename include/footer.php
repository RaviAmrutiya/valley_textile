<footer class="footer-content">
    <div class="layer footer">
        <div class="container-fluid">
            <div class="row footer-top-inner-w3ls">
                <div class="col-lg-4 col-md-6 footer-top ">
                    <h2>
                        <a href="index.php">Valley Textile</a>
                    </h2>
                    <p class="my-3" style="text-align: justify;"> As one of the countries’s most trusted suppliers of narrow woven fabric,
                        particularly Hook and loop tape fasteners.</p>
                </div>
                <div class="col-lg-4 col-md-6 mt-md-0 mt-5">
                    <div class="footer-w3pvt">
                        <h3 class="mb-3 w3pvt_title">Quick Links</h3>
                        <hr>
                        <ul class="list-info-w3pvt last-w3ls-contact mt-lg-3" style="font-size: 20px;">
                            <li class="my-2">
                                <a href="blog_post.php">
                                    <p><i class="fa fa-arrow-right" aria-hidden="true">&nbsp; Blog & Post</i></p>
                                </a>
                            </li>
                            <li class="my-2">
                                <a href="contact.php">
                                    <p><i class="fa fa-arrow-right" aria-hidden="true">&nbsp; Contact Us</i></p>
                                </a>
                            </li>
                            <li class="my-2">
                                <a href="application.php">
                                    <p><i class="fa fa-arrow-right" aria-hidden="true">&nbsp; Application</i></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-lg-0 mt-5">
                    <div class="footer-w3pvt">
                        <h3 class="mb-3 w3pvt_title">Contact Us</h3>
                        <hr>
                        <div class="last-w3ls-contact">
                            <p>
                                <h4><a href="mailto:valleytextile1@gmail.com"
                                        style="text-decoration: none;">valleytextile1@gmail.com</a></h4>
                            </p>
                        </div>
                        <div class="last-w3ls-contact my-2">
                            <a href="tel:+91 9687523723">
                                <p><b>Address :</b>  <br> Hadmatiya Road,Near Lajai Chokdi,At. Lajai,Ta. Tankara,Dis. Morbi (Guj.) | IND  </p>
                            </a>
                        </div>
                        <div class="last-w3ls-contact my-2">
                            <a href="tel:+91 9978699915">
                                <p>+91 9978699915</p>
                            </a>
                        </div>
                        <div class="last-w3ls-contact my-2">
                            <a href="tel:+91 9687523723">
                                <p>+91 9687523723</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="copy-right-grids text-li text-center my-sm-4 my-4" style="text-decoration: none; color : white">2020
            All Rights Reserved | Design by
            <b><a target="_blank" href="http://4foxwebsolution.com" target="_blank"
                    style="text-decoration: none; color : #ffc80a">4fox web
                    solution </a></b></h4><br>
         <div class="w3ls-footer text-center mt-4">
            <ul class="list-unstyled w3ls-icons">
                <li>
                    <a href="https://www.facebook.com/Valley-Textile-1748438168753083/" target="_blank">
                        <span class="fa fa-facebook-f"></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/valley_textile" target="_blank">
                        <span class="fa fa-instagram-f"></span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <span class="fa fa-linkedin-square-f"></span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <span class="fa fa-twitter-f"></span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <span class="fa fa-pinterest-f"></span>
                    </a>
                </li>
            </ul>
        </div> <br>
    
        <div id="WAButton"></div>

    </div>

</footer>
<script>
$(function() {
    $('#WAButton').floatingWhatsApp({
        phone: '+91 9687523723', //WhatsApp Business phone number International format-
        //Get it with Toky at https://toky.co/en/features/whatsapp.
        headerTitle: 'Chat with us on WhatsApp!', //Popup Title
        popupMessage: 'Hello!!! How can we help you?', //Popup Message
        showPopup: true, //Enables popup display
        buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
        //headerColor: 'crimson', //Custom header color
        //backgroundColor: 'crimson', //Custom background button color
        position: "right",
        size: "50px"
    });
});
</script>