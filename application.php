<?php
include('include/header.php');
?>


<!-- banner -->
<section class="inner-page-banner" id="home">
</section>
<!-- //banner -->
<!-- page details -->
<div class="breadcrumb-agile">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Application </li>
    </ol>
</div>
<!-- //page details -->
<!--about-mid -->

<section class="banner-bottom py-5" id="exp">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Application</h3>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col-md-4" id="carousel-tilenav" data-interval="false" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/Abrasives-Market.jpg"  class="card-img-top img-height" alt="Valley Textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Abrasives</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >We understand the value of strength and durability which are prime factors
                            for abrasives and industrial use.</p><br>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/Apparels.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Apparels</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">We offer hook and loop closure and other innovative products keeping style
                            and comfort in mind, which are two important factors for the apparel industry.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/car (2).jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Automotive</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">We understand the importance of innovation and efficiency in the automotive
                            industry. Therefore, we provide hook and loop straps for different types of applications
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row row-cols-1 row-cols-md-3" >
        <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card" >
                    <img src="images/avition.jpg"  class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Aviation</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">We offer high quality, innovative, and standard hook and loop fastening
                            solutions to meet specific requirements and applications in the aviation industry.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/cable-networking.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Cable Networking</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Our hook and loop wraps can be used to organize and tie your long, tangled
                            cables and wires.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/defence.png" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Defense</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Hook and loop fastening tapes for defense and military wear are
                            strong in strength and reliable for security.</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row row-cols-1 row-cols-md-3">
        <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/warehouse velcro.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body"><b>
                            <center><h3 class="card-title">Ware House</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Hook and loop fasteners and straps are strong and durable to pack and
                            store goods safely. They are easy to use and Reusable</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/footwear.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Footwear</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">hook and loop fasteners are carefully designed with durability and
                            flexibility which represent two key factors for the footwear industry.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/home furnishing.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Home Furnishing</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Unnapped Loop is an unbrushed loop material that is ideal for products
                            and applications which require a higher life cycle.</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row row-cols-1 row-cols-md-3">
        <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/hygiene.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Hygiene</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >We are the official marketing partner of Aplix in India. We provide a wide
                            range of hook and loop fasteners for hygiene that are soft and gentle for baby skin.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/infantwear velcro.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Infant Wear</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Valley provide hook and loop for diapers and other infant wear and
                            accessories.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/luggage.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Luggage</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >We provide different hook and loop straps to keep your luggage safe and
                            secure.</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row row-cols-1 row-cols-md-3">
        <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/media.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 200px;"><b>
                            <center><h3 class="card-title">Media</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">Valley's self- adhesive hook and loop range of products are used as a great
                            display tool.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/medical-equipment.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 200px;"><b>
                            <center><h3 class="card-title">Medical Equipments</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">We provide various hook and loop fasteners used in medical equipment.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/packaging.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 200px;"><b>
                            <center><h3 class="card-title">Packaging</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;">Our hook and loop fasteners ensure complete closure and long life cycle
                            which are two important key factors for packaging.</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row row-cols-1 row-cols-md-3">
        <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/ppe.png" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">PPE</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Our hook and loop material works as a shield and has a slow-burning speed
                            which is most suitable for personal and protective equipment.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/sports.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Sports</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >Hook and loop fastening solutions for sportswear are rested on
                            strength, fit, and durability which can be trusted upon.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="margin-bottom: 30px;">
                <div class="card">
                    <img src="images/stationary.jpg" class="card-img-top img-height" alt="valley textile">
                    <div class="card-body" style="height: 230px;"><b>
                            <center><h3 class="card-title">Stationery</h3></center>
                        </b>
                        <p class="card-text" style="text-align: justify;" >We provide hook and loop tapes for office, stationery, art, and craftwork.
                            It works as an alternative to glue, nails, and staples. It is hassle-free and easy to use.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!-- <div  class="row row-cols-1 row-cols-md-3">
        
            
        </div> -->
    </div>
</section>
<!-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="images/ban1.jpg" class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="images/ban3.jpg" class="d-block w-100" alt="...">
      </div>
      <div class="carousel-item">
        <img src="ban4.jpg" class="d-block w-100" alt="...">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div> -->


<!-- //about-mid -->
<!-- states -->


<!--//team -->
<!--//team -->
<!-- footer -->
<?php include("include/footer.php"); ?>

<!-- //footer -->



</body>

</html>
<!-- <script>
$('.carousel').carousel({
  interval: 6000
})
</script> -->