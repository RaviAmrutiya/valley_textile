<?php
include('include/header.php');
?>
<br><br><br><br>
<!-- slider section for floating slider  -->
    <section class="w3l-banner-3-main" style="margin-top:10px">
        <div class="banner-3">
            <!-- <img src="images/36.jpg" alt=""> -->
            <div class="wrapper">
                <div class="cover-top-center-9">
                    <div class="w3ls_cover_txt-9">
                        <h3 class="title-cover-9">Welcome To Valley Textile</h3>
                        <p class="para-cover-9">We have gained recognition as a specialist in providing
                                    hook and loop fastening solutions and many more...</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- slider section ends here  -->
<!-- banner -->
<!-- <div class="banner_w3lspvt" id="home">
    <div class="csslider infinity" id="slider1">
        <input type="radio" name="slides" checked="checked" id="slides_1" />
        <input type="radio" name="slides" id="slides_2" />
        <input type="radio" name="slides" id="slides_3" />
        <input type="radio" name="slides" id="slides_4" />

        <ul class="banner_slide_bg">
            <li>
                <div class="slider-info bg1">
                    <div class="bs-slider-overlay">
                        <div class="banner-text">
                            <div class="container">
                                <h2 class="movetxt agile-title text-capitalize" >Welcome 
                                <br>    
                                To Valley Textile
                                </h2>
                                 <p>
                                    We have gained recognition as a specialist in providing
                                    hook and loop fastening solutions and many more...</p> -->
                            <!-- </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-info bg2">
                    <div class="bs-slider-overlay1">
                        <div class="banner-text">
                            <div class="container">
                                <h4 class="movetxt agile-title text-capitalize">We have served to some of the most
                                    reputed companies from various Industries
                                </h4>
                                <p>ISO, IATF, OEKOTEX certified.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-info bg1">
                    <div class="bs-slider-overlay">
                        <div class="banner-text">
                            <div class="container">
                                <h2 class="movetxt agile-title text-capitalize">Welcome To Valley Textile
                                </h2>
                                <p>
                                    We have gained recognition as a specialist in providing
                                    hook and loop fastening solutions and many more...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-info bg1">
                    <div class="bs-slider-overlay">
                        <div class="banner-text">
                            <div class="container">
                                <h2 class="movetxt agile-title text-capitalize">Welcome To Valley Textile
                                </h2>
                                <p>
                                    We have gained recognition as a specialist in providing
                                    hook and loop fastening solutions and many more...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </li> -->
        <!-- </ul>
        <div class="navigation">
            <div>
                <label for="slides_1"></label>
                <label for="slides_2"></label>
                <label for="slides_3"></label>
                <label for="slides_4"></label>
            </div>
        </div>
    </div>
</div>  -->
<!-- //banner -->
<!-- banner bottom grids -->
<section class="content-info py-5" id="about">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">About us</h3>

        <div class="info-w3pvt-mid text-center px-lg-5">

            <div class="title-desc text-center px-lg-5">
                <img src="images/valley.png" alt="news image" class="img-fluid">
                <h4>Hook and Loop Manufacturer and Supplier since 2014</h4><br>
                <p class="px-lg-5" style="text-align: justify;">We Introduce Ourselves As Ane of India’s Leading Player in Narrow Woven Fabrics,
                    (Technical Textiles). As One of the Countries’s Most Trusted Suppliers of Narrow Woven Fabric,
                    Particularly Hook And Loop Tape Fasteners, We Have Gained Recognition As Specialist In Providing
                    Fastening Solutions.</p><br><br>
                <p style="text-align: justify;">Valley Textile Has Worked Closely With Some of The Most Reputed Companies Spread Across Various
                    Industry Segments In Footwear, Orthopedic, Defence, Textiles, Automotive, etc., And Aow Offers
                    Customers An Unmatched Array of Fastening Solutions And Products – We Cater To Every Need And
                    Deliver Fast, Reliable And Quality Solutions. Our Dedicated And Experienced Sales Staff Is Primed To
                    Listen To Our Client’s Every Request And Tailor The Best Possible Solution/Product Offering At The
                    Quickest Turn Around. Our Goal For Our Clients Is Not Just Maximizing Revenue But Also Expanding
                    Their Customer Base And Consequently Their Business, Leading To Long-Term Sustainable Growth. We Are
                    Confident That Given Your Support And Best Wishes We Will Progress To The Zenith Of Hook & Loop Tape
                    Industry Also Manufacturer Of Inject Hook / Molded Hook</p>
            </div>
        </div>
    </div>
</section>
<!-- //banner bottom grids -->

<!-- /services -->
<section class="services py-5" id="services">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Industries We serve</h3>
        <div class="row ab-info">
            <div class="col-md-6 ab-content ab-content1">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/footwear.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Footwear</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ab-content ab-content1">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/defence.png" alt="news image" class="img-fluid"
                            style="height: 340px;"></a>
                    <div class="ab-info-con">
                        <h4>Defence</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ab-info second mt-lg-4">
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/luggage.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Luggage</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/Abrasives-Market.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Abrasives</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/orthopadic.jpg" alt="news image"
                            class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Orthopedic</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/medical-equipment.jpg" alt="news image"
                            class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Medical Equipment</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ab-info second mt-lg-4">
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/car (2).jpg" alt="news image"
                            class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>automotive</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/sports.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Sports</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/ppe.png" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>PPE</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/packaging.jpg" alt="news image"
                            class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Packaging</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ab-info second mt-lg-4">
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/Apparels.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Apparels</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/avition.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Aviation</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/infantwear velcro.jpg" alt="news image" class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Hygiene</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ab-content">
                <div class="ab-content-inner">
                    <a href="javascript:void(0);"><img src="images/home furnishing.jpg" alt="news image"
                            class="img-fluid"></a>
                    <div class="ab-info-con">
                        <h4>Home Furnishing</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /services -->
<!--/order-now-->
<!-- <section class="order-sec py-5">
    <div class="container py-md-5">
        <div class="test-info text-center">
            <h3 class="tittle order">
                <span>CALL US TO BOOK AN APPOINTMENT</span>
            </h3>
            <h2 style="color:white;" >+91 9978699915 </h2>
        </div>
</section> -->
<!--//order-now-->
<!-- footer -->

<!-- footer -->
<?php include("include/footer.php"); ?>
<!-- //footer -->
</body>

</html>