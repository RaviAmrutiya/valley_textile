<?php
include('include/header.php');
?>


<!-- banner -->
<section class="inner-page-banner" id="home">
</section>
<!-- //banner -->
<!-- page details -->
<div class="breadcrumb-agile">
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item">
            <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Hook And Loop </li>
    </ol>
</div>
<!-- //page details -->
<!--about-mid -->

<section class="banner-bottom py-5" id="exp">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Hook And Loop Tape</h3>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;" >
                    <img src="images/hector.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">HECTOR BRAND</h3>
                            </b>
                            <p class="card-text"></p>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel1">
                                View More
                            </button>
                        </center>
                        <!-- Modal -->
                        <div class="modal fade" id="mymodel1" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">HECTOR BRAND
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body model-justify ">
                                        <center>
                                            <h4><b>Hook and Loop Regular Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Hector is our economy range of sew on hook and loop Tape. It is our regular
                                            fastener which is ruling the market and plays a major role in providing hook
                                            and loop fastening to major industries like Footwear, Luggage,Textile, Apparels, and many
                                            more.
                                        </p><br>
                                        <p><h5><b> CATEGORIES:</b></h5>  <br>Footwear, Apparels, Luggage</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5>
                                             <br> Standard Colors – Black & White. 
                                             <br>Available Size : 20mm , 25mm , 38mm , 50mm </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/top_grip.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">TOP GRIP BRAND</h3>
                            </b>
                            <p class="card-text"></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel2">
                                View More
                            </button>
                        </center>
                        <!-- Modal -->
                        <div class="modal fade" id="mymodel2" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">TOP GRIP BRAND</h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Hook and Loop High Strangth Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Top Grip Brand is our Regular hook and loop tape fastener which can be
                                            used
                                            when
                                            a strong grip is required. It is used in various industries like Sports
                                            Wear, Personal Protective Equipment,
                                            Footwear,
                                            Aviation, Apparels, and many more.
                                        </p><br>
                                        <p> <h5><b> CATEGORIES:</b></h5> <br> AUTOMOTIVE, AVIATION, FOOTWEAR, HOME
                                            FURNISHING, PPE, SPORTS</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors – Black & White.
                                        <br>Available Size : 10mm , 12mm , 16mm , 20mm , 25mm , 30mm , 32mm , 38mm , 50mm , 75mm ,100mm , 110mm , 125mm</p><br>
                                        <p><h5><b>TECH SPECS:</b></h5> <br> Peel and Shear greater than regular hook & loop and
                                            Magic HS works effortlessly for longer cycles.<br>Meets the parameters of
                                            IS-8156</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/antique.jpg" class="card-img-top" alt="Fire Retardant Hook and Loop">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">ANTIQUE BRAND</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel3">
                                View More
                            </button>
                        </center>
                        <!-- Modal -->
                        <div class="modal fade" id="mymodel3" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">ANTIQUE BRAND
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>High Grip Hook And Loop Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Antique Brand is our High Grip Hook And Loop fastener.It is 40% Nylon And 60% Polyester Material. It is  also Redying Materials Where it is Use in :                                           across different industries with applications like personal and safety
                                            Equipment, Defense ,
                                            and Military wear, Orthopedic , Footwear , PPE , Automobiles , Midea , Sports 
                                            and Work Wear Garments etc....
                                        </p><br>
                                        <p> <h5><b> CATEGORIES:</b></h5> <br> AVIATION, DEFENSE</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors – Black & White <br>Dye-To-Match
                                            colors for Coustomer requirement. </p><br><p> <h5><b> Available Size :</b></h5><p> 12mm , 16mm , 20mm , 25mm , 30mm ,32mm , 38mm , 50mm , 75mm , 100mm ,110mm , 125mm</p>
                                        <p><h5><b>TECH SPECS:</b></h5> <br>
                                            Test Certificate available upon request.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row row-cols-1 row-cols-md-3">
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/expert.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">EXPERT BRAND</h3>
                            </b>
                            <p class="card-text"></p>

                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel4">
                                View More
                            </button>
                        </center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel4" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">EXPERT BRAND
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Extremely High Grip</b></h4>
                                        </center> <br>
                                        <p>
                                            Expert Brand is High Strength and Long Life Cycle Hook And Loop. It is 50% Nylon And 50% Polyester Material.
                                            Best Suitable 
                                             Footwear industries , Orthopedic , Sports , Defense 
                                        </p><br>
                                        <p> <h5><b> CATEGORIES:</b></h5> <br> Defense, Footwear, 
                                        Orthopedic</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors – Black & White <br><h5> <b> Available Size : </b></h5> 20mm , 25mm , 38mm , 50mm , 100mm </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/ronak.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">RONAK BRAND</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel5">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel5" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">RONAK BRAND</h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>100% Nylon Hook And Loop</b></h4>
                                        </center> <br>
                                        <p>
                                            Ronak Brand  is Premium Product. it's Life Cycle is Very High Compair to Expert Brand.
                                            It is 100% Nylon Material.
                                        </p><br>
                                        <p><h5> <b> CATEGORIES:</b>
                                        
                                        </h5> <br> Defense, Footwear, Aviation , 
                                            Orthopedic</p><br>
                                            <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors – Black & White <br>Dye-To-Match
                                            colors for Coustomer requirement. </p>
                                            <p><p><h5> <b> Available Size : </b></h5> 20mm , 25mm , 38mm , 50mm</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/antique.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">ANTIQUE COLOR BRAND</h3>
                            </b>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel6">
                                View More
                            </button></center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel6" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">ANTIQUE COLOR BRAND</h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>High Grip Hook And Loop Tape</b></h4>
                                        </center> <br>
                                        <p>
                                            Antique Brand is our High Grip Hook And Loop fastener.It is 40% Nylon And 60% Polyester Material. It is  also Redying Materials Where it is Use in : across different industries with applications like personal and safety
                                            Defense ,
                                            and Military wear, Orthopedic , Footwear , Automobiles , Sports 
                                            and Work Wear Garments etc....
                                        </p><br>
                                        <p> <h5><b> CATEGORIES:</b></h5> <br> AVIATION, DEFENSE</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors –<br> Multiple Options for Coustomer requirement.</p><br><p> <h5><b> Available Size :</b></h5><p>  16mm , 20mm , 25mm , 38mm , 50mm , 75mm , 100mm ,110mm , 125mm</p>
                                        <p><h5><b>TECH SPECS:</b></h5> <br>
                                            Test Certificate available upon request.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row row-cols-1 row-cols-md-3" style="margin-top: 20px;">
            <div class="col-md-4">
                <div class="card" style="margin-bottom: 20px;">
                    <img src="images/hook_look.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <center>
                            <b>
                                <h3 class="card-title">Color Hook & Loop Tape</h3>
                            </b>
                            <p class="card-text"></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mymodel7">
                                View More
                            </button>
                        </center>

                        <!-- Modal -->
                        <div class="modal fade" id="mymodel7" data-backdrop="static" data-keyboard="false" tabindex="-1"
                            role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h1 class="modal-title w-100" id="staticBackdropLabel">Color Hook & Loop Tape
                                        </h1>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body model-justify">
                                        <center>
                                            <h4><b>Regular Hook and Loop Fastener</b></h4>
                                        </center> <br>
                                        <p>
                                            Color Hook & Loop Tape Hook and Loop fasteners treated with
                                            Antimicrobial
                                            Finish provides various
                                            benefits of controlling products from staining, discolouration, quality
                                            deterioration and odour formation caused by microbial contamination.
                                        </p><br>
                                        <p> <h5><b> CATEGORIES:</b></h5> <br> DEFENSE, FOOTWEAR, HOOK AND LOOP, LUGGAGE,
                                            ORTHOPEDIC</p><br>
                                        <p><h5><b>AVAILABILITY:</b></h5> <br> Standard Colors – Black & White <br>Dye-To-Match
                                            colors available upon request </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</section>
<!-- //about-mid -->
<!-- states -->
<!--//states -->

<!--//team -->
<!-- <section class="banner-bottom  py-5">
    <div class="container py-md-5">
        <h3 class="heading text-center mb-3 mb-sm-5">Our Team</h3>
        <div class="row mt-lg-5 mt-4">
            <div class="col-md-4 team-gd text-center">
                <div class="team-img mb-4">
                    <img src="images/t1.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3">JAMES Men spa</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>

            <div class="col-md-4 team-gd second text-center my-md-0 my-5">
                <div class="team-img mb-4">
                    <img src="images/t2.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3">DEEN MUSTACHIO</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-4 team-gd text-center">
                <div class="team-img mb-4">
                    <img src="images/t3.jpg" class="img-fluid" alt="user-image">
                </div>
                <div class="team-info">
                    <h3 class="mt-md-4 mt-3"> CLINT HAIRISTA</h3>
                    <p>Lorem Ipsum has been the industry's standard since the 1500s.</p>
                    <ul class="list-unstyled team-icons mt-4">
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="t-icon">
                                <span class="fa fa-dribbble"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

    </div>
</section> -->
<!--//team -->
<!-- footer -->
<?php include("include/footer.php"); ?>
<!-- //footer -->
</body>

</html>